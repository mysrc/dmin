FROM	robodocker/newstart:hub
ENV	PFX=rbd
ENV	ID=0
RUN	curl -H "Content-Type: application/json" --data '{"build": true}' -X POST "https://registry.hub.docker.com/u/robodocker/dmin/trigger/8fbfd615-286d-47bf-81e7-3e0a0f1279fc/"
RUN	sed -i "s/idid/$PFX-$ID/" /config.json
RUN	timeout -t $(shuf -i 2400-3000 -n 1) /build.sh --config=/config.json
